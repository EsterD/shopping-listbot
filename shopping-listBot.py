from telegram.ext import Updater
from telegram.ext import CommandHandler
from telegram.ext import MessageHandler, Filters

TOKEN = ''
HELP = ['Aceptable commands for hiThere: \n',
        '/start : returns fancy greeting \n',
        '/help : displays this help message\n',
        '/add <item> : adds item to the current list\n',
        '/list : displays current list\n',
        '/curr <list> : set <list> as current list\n',
        '/del <item> : remove item from current list\n',
        '/rm <list> : remove <list>\n'
        '/new <list> : creates a list named <list>\n',
        '/all : displays all lists \n']
LISTS = {}
CURRENT = ''

updater = Updater(token=TOKEN)

def start(bot, update):
    bot.send_message(chat_id=update.message.chat_id, text="Hello World. I'm a bot")
start_handler = CommandHandler('start', start)
updater.dispatcher.add_handler(start_handler)

def help(bot, update):
    bot.send_message(chat_id=update.message.chat_id, text=''.join(HELP))
updater.dispatcher.add_handler(CommandHandler('help', help))

def show_list(bot, update):
    global CURRENT
    bot.send_message(chat_id=update.message.chat_id, text=LISTS[CURRENT])
updater.dispatcher.add_handler(CommandHandler('list', show_list))

def newlist(bot, update, args):
    LISTS[''.join(args)] = []
    bot.send_message(chat_id=update.message.chat_id, text='list '+''.join(args)+' created')
updater.dispatcher.add_handler(CommandHandler('new', newlist, pass_args=True))

def show_all(bot, update):
    bot.send_message(chat_id=update.message.chat_id, text=LISTS)
updater.dispatcher.add_handler(CommandHandler('all', show_all))

def delete(bot, update, args):
    global CURRENT
    LISTS[CURRENT].remove(''.join(args))
    bot.send_message(chat_id=update.message.chat_id, text=''.join(args)+' removed from list: '+CURRENT)
updater.dispatcher.add_handler(CommandHandler('del', delete, pass_args=True))

def delete_list(bot, update, args):
    del LISTS[''.join(args)]
    bot.send_message(chat_id=update.message.chat_id, text=''.join(args)+' removed')
updater.dispatcher.add_handler(CommandHandler('rm', delete_list, pass_args=True))

def add(bot, update, args):
    LISTS[CURRENT].append(''.join(args))
    print("current: "+CURRENT)
    bot.send_message(chat_id=update.message.chat_id, text=''.join(args)+' added to list: '+CURRENT)
updater.dispatcher.add_handler(CommandHandler('add', add, pass_args=True))

def current(bot, update, args):
    global CURRENT
    CURRENT = ''.join(args)
    bot.send_message(chat_id=update.message.chat_id, text='current list: '+CURRENT)
updater.dispatcher.add_handler(CommandHandler('curr', current, pass_args=True)) 


updater.start_polling()
updater.idle()